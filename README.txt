-- SUMMARY --

Cheat Sheet Generator

This module provides functionality to create drupal cheatsheets.

Cheatsheets:
  - Advanced hooklist
  - ..more to come
  - ..whishes? go ahead and create an issue:
    http://drupal.org/project/cheatsheet

Use case? See my blogpost: http://ventral.org/blog/cheat-sheet-generator

-- REQUIREMENTS --

none.


-- How to use it? --

Install as usual, see http://drupal.org/node/895232 for further information.

Configuration
  No configuration needed.

Usage
  Goto admin/reports/cheatsheet and configure what and how you want your
  cheatsheet.
